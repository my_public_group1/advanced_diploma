# Docker-GO-diploma

[![Build Status](https://gitlab.com/my_public_group1/basic_diploma/badges/main/pipeline.svg)](https://gitlab.com/my_public_group1/basic_diploma)

Данный проект является дипломной работой курса "DevOps-инженер. Advanced" и содержит в себе следующие этапы:

- Создание инфраструктуры в облаке Amazon AWS с помощью инструмента Terraform;
- Конфигурация инфраструктуры и deploy приложения с помощью Ansible и Docker;
- Тестирование приложения и запуск плейбука Ansible с помощью Gitlab CI/CD.

## Требования для запуска проекта:

1. Зарегистрируйте аккаунт [Amazon AWS](https://aws.amazon.com/account/sign-up);
2. Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) для управления аккаунтом и ресурсами;
3. Перейдите в `Identity and Access Management (IAM)` создайте пользователя `Administrator`, создайте группу `Administrators` с политикой доступа `AdministratorAccess` и добавьте только что созданного пользователя в эту группу;
4. Далее перейдите в вашего пользователя `Administrator`, выберите вкладку `Security Credentials`, создайте `Access Key`, и сохраните у себя `AWS Access Key ID` и `AWS Secret Access Key`;
5. [Cконфигурируйте AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html), используя раннее полученные значения `AWS Access Key ID` и `AWS Secret Access Key`;
3. Создайте [SSH-ключ](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/create-key-pairs.html) и скопируйте его к себе;
4. Купите домен у любого регистратора или зарегистрировать бесплатный, [создать hosted zone в AWS Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html) и [прописать NS-сервера у регистратора домена](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-name-servers-glue-records.html);
5. Зарегистрируйте аккаунт [PagerDuty](https://www.pagerduty.com/sign-up/), создайте сервис получения уведомлений с интеграцией Prometheus, и сохраните у себя url и приватный ключ;
6. Установите [Terraform](https://www.terraform.io/downloads);
7. Установите [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html);
8. Сохраните себе url репозитория gitlab с проектом;
9. Выполните команду для генерации секретного ключа Graylog и сохраните его у себя:
- `pwgen -N 1 -s 96`
10. Установите на своей рабочей или виртуальной машине Consul, не запуская сам агент. Далее сгенерируйте ключ Consul для дальнейшего сохранения у себя и использования при конфигурировании инфраструктуры. Для этого выполните следующие комманды:
- `curl -fsSL https://apt.releases.hashicorp.com/gpg |   sudo apt-key add -`
- `sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"`
- `sudo apt-get update && sudo apt-get install consul`
- `sudo consul keygen`

## Описание инфраструктуры проекта:

Инфраструктура проекта предусматривает создание следующих элементов: 

| Элемент | Назначение |
| --- | ----------- |
| [AWS ELB Server](https://aws.amazon.com/elasticloadbalancing/) | Необходим для балансировки нагрузки между серверами приложения |
| [AWS Auto Scaling Group](https://aws.amazon.com/autoscaling/) | Группа автоматического масштабирования серверов приложения в зависимости от нагрузки | 
| Monitoring Server | Необходим для мониторинга всех серверов инфраструктуры |
| Control Node Server | Используется для работы gitlab-runner, а также для динамического создания inventory файла |
| [AWS Route53](https://aws.amazon.com/route53/) Records | DNS записи для приложения и инструментов мониторинга в системе Route53 |
| [AWS OpenSearch](https://aws.amazon.com/opensearch-service/) | Аналитическая система, используемая для сбора и анализа различных журналов инфраструктуры |
| [Graylog Server](https://www.graylog.org/) | Сервер для сбора логов, создания алертов, и записи их в AWS OpenSearch |
| [Consul Servers](https://www.consul.io/) | Кластер из N-серверов, необходимый для работы Service Discovery, а именно для автоматического обнаружения серверов с зарегистрированными сервисами в кластере |
| [AWS S3 Bucket](https://aws.amazon.com/s3/) | Корзина AWS S3 используется для хранения бэкапов состояния Terraform, а также для хранения релизной версии проекта основной ветки main |
| [AWS DynamoDB](https://aws.amazon.com/dynamodb/) | NoSQL база данных, необходимая для хранения, записи, обработки состояний Terraform посредством корзины AWS S3 Bucket |

> Таблица 1.

Схема взаимодействия между элементами инфраструктуры представлена ниже:

![scheme_process](images/aws-iac.png)

Схема, показывающая порядок создания элементов инфраструктуры, представлена ниже:

![scheme_create](images/aws_create_steps.png)

Инфраструктура создается следующим образом:

1. На первом шаге проиходит запуск Terraform для создания AWS S3 Bucket, DynamoDB, объектов AWS S3 для хранения состояния Terraform и релизной версии проекта;

2. На втором шаге запускается Terraform для создания самой инфраструктуры. На данном шаге происходит разделение этапов создания на два параллельных потока.

Первый поток включает в себя следующие этапы:
- Запуск null_resource для удаления не актуального inventory файла с Consul серверами;
- Создаение Consul серверов;
- Запуск null_resource, который в свою очередь запускает Ansible-Playbook для конфигурации кластера на Consul серверах;
- Создаются и конфигурируются инстансы AWS Auto Scaling Group для работы приложения GO и Consul клиента.

Второй поток включает в себя следующие этапы:
- Cоздается домен AWS OpenSearch 
- Cоздается сервер GrayLog для сбора, обработки и дальнейшей отравки логов (журналов) на AWS OpenSearch. GrayLog собирает журналы с инстансов AWS Auto Scaling Group, c Control Node сервера, ELB сервера, а также с сервера мониторинга. Отправка журналов с серверов происходит с помощью иструмента [fluent-bit](https://fluentbit.io/);
- После создания сервера параллельно запускаются следующие процессы:
  - Запускается Ansible-Playbook для конфигурирования GrayLog сервера;
  - Запускается и конфигурируется AWS Elastic Load Balancer для балансировки нагрузки между инстансами автоматической группы масштабирования AWS Auto Scaling Group;
  - Создаётся сервер Control Node, который в свою очередь конфигурирует и запускает gitlab-runner, динамически формирует inventory файл Ansible с инстансами AWS Auto Scaling Group, а также запускает Ansible Playbook на эти инстансы;
  - Создается и конфигурируется сервер мониторинга, который собирает метрики с инстансов AWS Auto Scaling Group, c Control Node сервера, ELB сервера, GrayLog сервера, а также с самого себя;

Также Route53, после создания какого-либо ресурса, имеющего домен, создает этот домен.  

## Как работать с gitlab:

Необходимо сохранить у себя токен для создания gitlab-runner, для этого выполните следующие действия:

1. Перейдите `Settings -> CI/CD -> Runners`;

2. В области `Shared Runners` отключите флажок `Enable shared runners for this project`;

3. В области `Specific Runners` найдите строку `And this registration token` и сохраните у себя токен, который следует за этой строкой. Этот токен необходим для создания новых gitlab-раннеров;

4.  Далее необходимо перейти в общие настройки gitlab. Для этого необходимо нажать на аватар профиля справа сверху и выбрать `Preferences`. На открывшейся странице необходимо выбрать вкладку `Access Token`. Далее создаем новый токен, выбираем для него любое имя и права доступа `api`, `read_api`. После создания появится токен `Your new personal access token`, его необходимо скопировать себе и сохранить. Этот токен необходим для запуска скрипта удаления не активных gitlab-раннеров.

## Как работать с проектом:
1. Скопируйте SSH-ключ для управления AWS в директорию /var/www:

   ```shell
   cp /path/to/your/key /var/www/
   ```

2. Склонируйте репозиторий к себе для работы с ним:

   ```shell
   git clone git@gitlab.com:my_public_group1/advanced_diploma.git
   ```
3. Далее необходимо сконфигурировать и запустить Terraform для создания корзины AWS S3 Bucket. Перейдите в директорию `infra/s3-terraform` и переименуйте файл `main.tf.example` в `main.tf`, после отредактриуйте следующие переменные:

| Переменная | Значение |
| --- | ----------- |
| aws_region | Регион Amazon AWS, в котором необходимо создавать ресурсы |
| bucket_name | Название корзины AWS S3 Bucket (должно быть уникально среди пользователей AWS, иначе будет ошибка) |
| dynamodb_name | Название базы данных DynamoDB (должно быть уникально среди пользователей AWS, иначе будет ошибка) |

> Таблица 2.

4. Инициализируйте Terraform и запустите создание ресурсов:

   ```shell
   terraform init
   terraform apply
   ```
5. Далее необходимо сконфигурировать и запустить Terraform для создания инфраструктуры проекта. Перейдите в директорию `infra/terraform` и переименуйте файл `main.tf.example` в `main.tf`, после отредактриуйте следующие переменные:

| Переменная | Значение |
| --- | ----------- |
| route53_hosted_zone_name | Ваш домен, зона которого была сконфигрурирована в Route 53 |
| aws_keyname | название SSH-ключа для управления ресурсами AWS без формата файла |
| ssh_key_private | Полный путь до SSH-ключа для управления ресурсами AWS с полным названием файла |
| os_user | Имя пользователя для входа в OpenSearch |
| os_pass | Пароль для входа в OpenSearch |
| grafana_user | Имя пользователя для входа в grafana |
| grafana_pass | Пароль для входа в grafana |
| gitlab_token | Токен для создания gitlab-раннера |
| gitlab_access_api_token | Токен для доступа к API gitlab |
| pagerduty_user_url | PagerDuty URL |
| pagerduty_user_key | PagerDuty Key |
| consul_input_key | Ключ `Consul Raw Key` (полученный коммандой `consul keygen`) |
| gitlab_repo | Ссылка на репозиторий с данным проектом |
| graylog_secret | Ключ Graylog (`pwgen -N 1 -s 96`) |
| graylog_password | Пароль для входа в GrayLog |
| aws_access_key_id | ID ключа AWS CLI |
| aws_secret_access_key | Ключ AWS CLI |

> Таблица 3.

6. Инициализируйте Terraform с файлом конфигурации backend и создайте два рабочих пространства для тестового и продакшн окружений:

   ```shell
   terraform init -backend-config=backend.hcl
   terraform workspace new test
   terraform workspace new app
   ```

7. Выберите рабочие пространство для тестового окружения и запустите создание ресурсов AWS:

   ```shell
   terraform workspace select test
   terraform apply -var-file="s3.tfvars"
   ```

8. Далее необходимо выполнить аналогичные действия для создания продакшн окружения:

   ```shell
   terraform workspace select app
   terraform apply -var-file="s3.tfvars"
   ```

9. При создании инфраструктуры через Terraform, выполняется деплой приложения GO на инстансы AWS ASG с помощью локального запуска Ansible-Playbook посредством скрипта Bash. Приложение деплоится из ветки main, вне зависимости от того, в каком рабочем пространстве был запущен Terraform;

10. После создания ресурсов через `Terraform` создайте тестовую ветку с названием `uat-ddmmyy` выполните `commit` и `push` наших изменений на `gitlab.com` , что вызовет запуск `ansible playbook` для развертывания `docker` контейнеров с нашим `GO` приложением на тестовое окружение:

   ```shell
   git checkout -b uat-ddmmyy
   git add .
   git commit -m "init_test_commit"
   git push --set-upstream origin uat-221022
   ```

11. Далее, если в тестовом окружении все выполнилось без ошибок, и поднялось наше приложение, необходимо создать запрос на слияние (merge request) тестовой ветки и основной `main` на `gitlab.com`, где содержится код приложения. После подверждения данного запроса будет выполнен запуск ansible playbook для развертывания `docker` контейнеров с нашим `GO` приложением на продакшн окружение.

12. Результат выполнения можно увидеть после выполнения pipeline по следующим субдоменам `(http и https доступны)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | app.example.com |
| test | test.example.com |

> Таблица 4.

13. Мониторинг доступен по различным адресам, в зависимости от окружения `(доступен только http)`:

| Окружение | Инструмент мониторинга | Субдомен |
| --- | ----------- | ----------- |
| app | Prometheus | prom.example.com |
|| Grafana | grafana.example.com |
|| Alertmanager | am.example.com |
|| VictoriaMetrics | victoria.example.com |
| test | Prometheus | prom-test.example.com |
|| Grafana | grafana-test.example.com |
|| Alertmanager | am-test.example.com |
|| VictoriaMetrics | victoria-test.example.com |

> Таблица 5.

14. Дэшборд OpenSearch доступен по следующим доменам `(https доступен)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | os.example.com/_dashboards |
| test | os-test.example.com/_dashboards |

> Таблица 6.

15. Дэшборд GrayLog доступен по следующим доменам `(доступен только http)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | graylog.example.com |
| test | graylog-test.example.com |

> Таблица 7.
