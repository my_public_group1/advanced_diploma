import java.nio.charset.StandardCharsets;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

class Main {
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException {
    String  originalPassword = "admin";

    byte [] saltForHash = PBKDF2WithHmacSHA512.salt();
    byte [] hashForSonar =  PBKDF2WithHmacSHA512.hash(originalPassword, saltForHash);

    String generatedSecuredPasswordHash = new String(hashForSonar, StandardCharsets.UTF_8);
    System.out.println(generatedSecuredPasswordHash);
    }
}
