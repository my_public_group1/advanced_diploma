import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.math.BigInteger;

class Main {
    public static void main(String[] args) 
        throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String  originalPassword = "admin";
    
        String generatedSecuredPasswordHash 
            = generateStorngPasswordHash(originalPassword);
        System.out.println(generatedSecuredPasswordHash);
    }
    private static String generateStorngPasswordHash(String password) 
        throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        int iterations = 100000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();
    
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 384);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
    
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }
    
    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
    
    private static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }
}
