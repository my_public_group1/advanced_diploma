resource "aws_iam_role" "ec2_access_role" {
  name               = "ec2-role-${terraform.workspace}"
  assume_role_policy = "${file("./files/assumerolepolicy.json")}"
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name  = "ec2-profile-${terraform.workspace}"                         
  role = "${aws_iam_role.ec2_access_role.name}"
}

resource "aws_iam_policy" "ec2_policy" {
  name        = "ec2-policy-${terraform.workspace}"
  description = "EC2 full access policy"
  policy      = "${file("./files/policyec2full.json")}"
}

resource "aws_iam_policy" "cloudwatch_policy" {
  name        = "cloudwatch-policy-${terraform.workspace}"
  description = "CloudWatch full access policy"
  policy      = "${file("./files/cloudwatch_policy.json")}"
}

resource "aws_iam_policy_attachment" "ec2_attach" {
  name       = "ec2-attachment-${terraform.workspace}"
  roles      = ["${aws_iam_role.ec2_access_role.name}"]
  policy_arn = "${aws_iam_policy.ec2_policy.arn}"
}

resource "aws_iam_policy_attachment" "cloudwatch_attach" {
  name       = "cloudwatch-attachment-${terraform.workspace}"
  roles      = ["${aws_iam_role.ec2_access_role.name}"]
  policy_arn = "${aws_iam_policy.cloudwatch_policy.arn}"
}
