resource "aws_iam_service_linked_role" "opensearch" {
  aws_service_name = "opensearchservice.amazonaws.com"
  provisioner "local-exec" {
    command = "sleep 10"
  }
}

resource "aws_iam_service_linked_role" "asg" {
  aws_service_name = "autoscaling.amazonaws.com"
  description      = "A service linked role for autoscaling"
  custom_suffix = "other"
  provisioner "local-exec" {
    command = "sleep 10"
  }
}

