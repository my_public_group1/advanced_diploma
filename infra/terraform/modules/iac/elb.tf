# Elastic Load Balancer проксирует трафик на наши сервера 
resource "aws_elb" "web" {
  name               = "Docker-ELB-${terraform.workspace}"
  # перенаправляет трафик на несколько Дата центров
  availability_zones = [data.aws_availability_zones.available.names[1], data.aws_availability_zones.available.names[2]]
  security_groups    = [aws_security_group.elb.id]
  # слушает на порту 80
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
  listener {
    instance_port      = 8080
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "${module.acm.acm_certificate_arn}"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 70
  }
  tags = {
    Name = "Docker-Highly-Available-ELB-${terraform.workspace}"
  }
}
