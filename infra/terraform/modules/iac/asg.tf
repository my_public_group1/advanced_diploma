# Создаём Launch Configuration - это сущность, которая определяет конфигурацию запускаемых серверов. Размер, , 
resource "aws_launch_configuration" "web" {
  name_prefix     = "DOCKER-${terraform.workspace}-"
  # какой будет использоваться образ
  image_id        = data.aws_ami.ubuntu.id
  # Размер машины (CPU и память)
  instance_type   = "t2.micro"
  # какие права доступа
  security_groups = [aws_security_group.asg.id]
  depends_on = [null_resource.run_ansible_consul_server]
  # какие следует запустить скрипты при создании сервера
  user_data       = templatefile("files/install_ansible.tftpl",
                           { server_ip = "${aws_instance.consul_server[0].private_ip}",
                             os_endpoint = "${aws_opensearch_domain.skill_study.endpoint}",
                             os_user = "${var.os_user}",
                             os_pass = "${var.os_pass}",
                             aws_region = "${var.aws_region}",
                             gitlab_repo = "${var.gitlab_repo}",
                             bucket_key_path = "${var.bucket_key_path_version}",
                             env_workspace = "${terraform.workspace}" })
  # какой SSH ключ будет использоваться 
  key_name = "${var.aws_keyname}"
  # Если мы решим обновить инстанс, то, прежде, чем удалится старый инстанс, который больше не нужен, должен запуститься новый
  lifecycle {
    create_before_destroy = true
  }
}


# AWS Autoscaling Group для указания, сколько нам понадобится инстансов 
resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web.name}-${terraform.workspace}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = 1
  max_size             = 4
  min_elb_capacity     = 1
  desired_capacity     = 1
  health_check_type    = "ELB"
  wait_for_capacity_timeout = "30m"
  service_linked_role_arn = aws_iam_service_linked_role.asg.arn
  # и в каких подсетях, каких Дата центрах их следует разместить
  vpc_zone_identifier  = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  # Ссылка на балансировщик нагрузки, который следует использовать 
  load_balancers       = [aws_elb.web.name]

  dynamic "tag" {
    for_each = {
      Name   = "Docker-ASG-${terraform.workspace}"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}
