# Создадим сертификат SSL
module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  zone_id = "${data.aws_route53_zone.zone.zone_id}"

  domain_name               = "${var.route53_hosted_zone_name}"
  subject_alternative_names = ["*.${var.route53_hosted_zone_name}"]

  wait_for_validation = true
}

# Привяжем SSL политику к ELB
resource "aws_lb_ssl_negotiation_policy" "elbpolicy" {
  name          = "elbpolicy-${terraform.workspace}"
  load_balancer = "${aws_elb.web.id}"
  lb_port       = "443"

  attribute {
    name  = "Protocol-TLSv1"
    value = "true"
  }

  attribute {
    name  = "Protocol-SSLv3"
    value = "false"
  }

  attribute {
    name  = "Protocol-TLSv1.1"
    value = "true"
  }

  attribute {
    name  = "Protocol-TLSv1.2"
    value = "true"
  }

  attribute {
    name  = "Server-Defined-Cipher-Order"
    value = "true"
  }

  attribute {
    name  = "ECDHE-ECDSA-AES128-GCM-SHA256"
    value = "true"
  }

  attribute {
    name  = "ECDHE-RSA-AES128-GCM-SHA256"
    value = "true"
  }

  attribute {
    name  = "ECDHE-ECDSA-AES128-SHA256"
    value = "true"
  }

  attribute {
    name  = "ECDHE-RSA-AES128-SHA256"
    value = "true"
  }

  attribute {
    name  = "ECDHE-ECDSA-AES128-SHA"
    value = "true"
  }

  attribute {
    name  = "ECDHE-RSA-AES128-SHA"
    value = "true"
  }

  attribute {
    name  = "DHE-RSA-AES128-SHA"
    value = "false"
  }

  attribute {
    name  = "ECDHE-ECDSA-AES256-GCM-SHA384"
    value = "true"
  }

  attribute {
    name  = "ECDHE-RSA-AES256-GCM-SHA384"
    value = "true"
  }

  attribute {
    name  = "ECDHE-ECDSA-AES256-SHA384"
    value = "true"
  }

  attribute {
    name  = "ECDHE-RSA-AES256-SHA384"
    value = "true"
  }

  attribute {
    name  = "ECDHE-RSA-AES256-SHA"
    value = "true"
  }

  attribute {
    name  = "ECDHE-ECDSA-AES256-SHA"
    value = "true"
  }

  attribute {
    name  = "AES128-GCM-SHA256"
    value = "true"
  }

  attribute {
    name  = "AES128-SHA256"
    value = "true"
  }

  attribute {
    name  = "AES128-SHA"
    value = "true"
  }

  attribute {
    name  = "AES256-GCM-SHA384"
    value = "true"
  }

  attribute {
    name  = "AES256-SHA256"
    value = "true"
  }

  attribute {
    name  = "AES256-SHA"
    value = "true"
  }
}

