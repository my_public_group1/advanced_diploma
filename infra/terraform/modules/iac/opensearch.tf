resource "aws_opensearch_domain" "skill_study" {
  domain_name    = "skull-study-${terraform.workspace}"
  engine_version = "OpenSearch_1.3"

  cluster_config {
    instance_count = 2
    instance_type = "r6g.large.search"
    zone_awareness_enabled = true
  }

  encrypt_at_rest {
    enabled = true
  }

  node_to_node_encryption {
    enabled = true
  }

  domain_endpoint_options {
    custom_endpoint_certificate_arn = "${module.acm.acm_certificate_arn}"
    custom_endpoint_enabled = true
    custom_endpoint = "os${local.env_domain}.${var.route53_hosted_zone_name}"
    enforce_https = true
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }

  advanced_security_options {
    enabled = true
    internal_user_database_enabled = true
    master_user_options {
      master_user_name = "${var.os_user}"
      master_user_password = "${var.os_pass}"
    }
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  tags = {
    Domain = "${var.route53_hosted_zone_name}"
  }

  depends_on = [module.acm, aws_iam_service_linked_role.opensearch]
}

resource "aws_opensearch_domain_policy" "skill_study" {
  domain_name = aws_opensearch_domain.skill_study.domain_name

  access_policies = <<POLICIES
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "es:*",
      "Resource": "${aws_opensearch_domain.skill_study.arn}/*"
    }
  ]
}
POLICIES
}

