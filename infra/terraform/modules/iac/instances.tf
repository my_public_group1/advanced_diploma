# Запускаем GrayLog сервер
resource "aws_instance" "graylog_server" {
  # с выбранным образом
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = "t2.2xlarge"
  vpc_security_group_ids = [aws_security_group.graylog_server.id]
  key_name = "${var.aws_keyname}"

  root_block_device {
    volume_size = 30
  }

  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "GrayLog Server (${terraform.workspace})"
  }

  depends_on = [
    aws_opensearch_domain.skill_study
  ]

#  enclave_options {
#    enabled = true
#  }

  provisioner "remote-exec" {
    inline = ["sudo apt-get update -y",
              "sudo apt-get install python3 -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      cat >${var.ans_path}/graylog/grayhosts${terraform.workspace}.inv <<EOL
      [consul_instances]
      ${self.public_ip}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/fluent_bit.yml' <<EOL
      graylog_endpoint: ${self.public_ip}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/consul_client.yml' <<EOL
      consul_join: ["${aws_instance.consul_server[0].private_ip}"]
      env_workspace: ${terraform.workspace}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/nginx.yml' <<EOL
      current_domain: ${var.route53_hosted_zone_name}
      env_domain: ${local.env_domain}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/cn.yml' <<EOL
      runner_tags: aws,docker,${terraform.workspace}
      gitlab_token: ${var.gitlab_token}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/os.yml' <<EOL
      os_endpoint: ${aws_opensearch_domain.skill_study.endpoint}
      os_user: ${var.os_user}
      os_pass: ${var.os_pass}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/monitoring.yml' <<EOL
      grafana_user: ${var.grafana_user}
      grafana_password: ${var.grafana_pass}
      pagerduty_user_url: ${var.pagerduty_user_url}
      pagerduty_user_key: ${var.pagerduty_user_key}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/graylog.yml' <<EOL
      graylog_password_secret: ${var.graylog_secret}
      graylog_password_unencrypted: ${var.graylog_password}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/sonarqube.yml' <<EOL
      sonar_password: ${var.sonar_password}
      psql_username: ${var.psql_username}
      psql_password: ${var.psql_password}
      EOL
    EOT
  }
}


# Запускаем инстанс control_node
resource "aws_instance" "my_cn" {
  # с выбранным образом ubuntu 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  iam_instance_profile = "${aws_iam_instance_profile.ec2_profile.name}"
  vpc_security_group_ids = [aws_security_group.control_node.id]
  key_name = "${var.aws_keyname}"

  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Control Node Server (${terraform.workspace})"
  }

  depends_on = [aws_instance.graylog_server]

  connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
  }

  provisioner "file" {
    content = <<-EOT
      os_endpoint: ${aws_opensearch_domain.skill_study.endpoint}
      os_user: ${var.os_user}
      os_pass: ${var.os_pass}
      aws_region: ${var.aws_region}
      bucket_name: ${var.bucket_name}
      bucket_key_path: ${var.bucket_key_path_version}
      aws_access_key_id_input: ${var.aws_access_key_id}
      aws_secret_access_key_input: ${var.aws_secret_access_key}
      aws_metaname: ${var.meta_name}
      graylog_endpoint: ${aws_instance.graylog_server.private_ip}
      repository: ${var.gitlab_repo}
    EOT
    destination = "/tmp/vars.yml"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt update -y",
              "sudo apt-get install python3 -y",
              "sudo mkdir /var/www",
              "sudo cp /tmp/vars.yml /var/www/vars.yml",
              "rm -f /tmp/vars.yml",
              "sudo chmod 400 /var/www/vars.yml"]
  }

  provisioner "local-exec" {
    command = <<-EOT
      files/remove_runners.sh '${var.gitlab_access_api_token}'
      cat >cnhosts${terraform.workspace}.inv <<EOL
      [consul_instances]
      ${self.public_ip}
      EOL
      ansible-playbook -u ubuntu -i cnhosts${terraform.workspace}.inv --private-key ${var.ssh_key_private} ../ansible/control-node/main.yml --extra-vars "def_path=${var.def_path} tf_workspace=${terraform.workspace}" -b -v
      rm -f cnhosts${terraform.workspace}.inv
    EOT
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Запускаем инстанс ELB
resource "aws_instance" "my_webserver" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.elb.id]
  key_name = "${var.aws_keyname}"
  tags = {
    Name  = "GO Server (${terraform.workspace})"
    Tier = "Backend"
    CM = "Ansible"
  }

  depends_on = [aws_instance.graylog_server]

  connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
  }  

  provisioner "remote-exec" {
    inline = ["sudo apt update -y",
              "sudo apt-get install python3 -y"]
  }

  provisioner "local-exec" {
    command = <<-EOT
      cat >elbhosts${terraform.workspace}.inv <<EOL
      [consul_instances]
      ${self.public_ip}
      EOL
      ansible-playbook -u ubuntu -i elbhosts${terraform.workspace}.inv --private-key ${var.ssh_key_private} ../ansible/elb/main.yml --extra-vars "def_path=${var.def_path} tf_workspace=${terraform.workspace}" -b -v
      rm -f elbhosts${terraform.workspace}.inv
    EOT
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Запускаем Prometheus сервер
resource "aws_instance" "mon_server" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.small"
  vpc_security_group_ids = [aws_security_group.mon_server.id]
  key_name = "${var.aws_keyname}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Monitoring Server (${terraform.workspace})"
  }

  depends_on = [aws_instance.graylog_server]

#  enclave_options {
#    enabled = true
#  }

  provisioner "remote-exec" {
    inline = ["sudo apt update -y",
              "sudo apt-get install python3 -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/monitoring/main.yml --extra-vars "def_path=${var.def_path} tf_workspace=${terraform.workspace}" -b -v
    EOT
  }
}


# Запускаем SonarQube сервер
resource "aws_instance" "sonar_server" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.2xlarge"
  vpc_security_group_ids = [aws_security_group.sonar_server.id]
  key_name = "${var.aws_keyname}"

  root_block_device {
    volume_size = 30
  }

  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "SonarQube Server (${terraform.workspace})"
  }

  depends_on = [aws_instance.graylog_server]

#  enclave_options {
#    enabled = true
#  }

  provisioner "remote-exec" {
    inline = ["sudo apt update -y",
              "sleep 10s",
              "sudo apt-get install python3 -y",
              "sudo apt-get install python3-pip -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      cat >sonarhosts${terraform.workspace}.inv <<EOL
      [consul_instances]
      ${self.public_ip}
      EOL
      ansible-playbook -u ubuntu -i sonarhosts${terraform.workspace}.inv --private-key ${var.ssh_key_private} ../ansible/sonarqube/main.yml --extra-vars "def_path=${var.def_path} tf_workspace=${terraform.workspace}" -b -v
      rm -f sonarhosts${terraform.workspace}.inv
    EOT
  }
}



resource "aws_instance" "consul_server" {
  count = 3
  # с выбранным образом
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.consul_server.id]
  key_name = "${var.aws_keyname}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Consul Server ${count.index} (${terraform.workspace})"
  }

  depends_on = [null_resource.remove_inventory_file]

  provisioner "remote-exec" {
    inline = ["sleep 15s", 
              "sudo apt-get update -y",
              "sleep 10s",
              "sudo apt-get install python3 -y",
              "sudo apt-get install python3-pip -y"]
    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      echo 'serv${count.index} ansible_ssh_host=${self.public_ip} ansible_ssh_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private} consul_bind_address=${self.private_ip} consul_client_address="${self.private_ip} 127.0.0.1"' >> ../ansible/consulhosts${terraform.workspace}.inv
    EOT
  }
}

