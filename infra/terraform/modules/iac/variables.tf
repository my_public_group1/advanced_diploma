variable "route53_hosted_zone_name" {
  type = string
}

variable "ssh_key_private" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "aws_keyname" {
  type = string
}

variable "allowed_cidr_blocks" {
  type = list
  default = ["0.0.0.0/0"]
}

variable "ssh_tcp_port" {
  type = list
  default = ["22"]
}

variable "http_tcp_port" {
  type = list
  default = ["80"]
}

variable "https_tcp_port" {
  type = list
  default = ["443"]
}

variable "node_exp_tcp_port" {
  type = list
  default = ["9100"]
}

variable "cadvisor_tcp_port" {
  type = list
  default = ["9280"]
}

variable "psql_exp_tcp_port" {
  type = list
  default = ["9187"]
}

variable "alertmanager_udp_port" {
  type = list
  default = ["9094"]
}

variable "alertmanager_tcp_ports" {
  type = list
  default = ["9093", "9094"]
}

variable "prom_tcp_port" {
  type = list
  default = ["9090"]
}

variable "grafana_tcp_port" {
  type = list
  default = ["3000"]
}

variable "victoria_tcp_port" {
  type = list
  default = ["8428"]
}

variable "cloudwatch_tcp_port" {
  type = list
  default = ["5000"]
}

variable "consul_tcp_ports" {
  type = list
  default = ["8600", "8301", "8302", "8500", "8501", "8502", "8300"]
}

variable "consul_udp_ports" {
  type = list
  default = ["8600", "8301", "8302"]
}

variable "service_tcp_port" {
  type = list
  default = ["8080"]
}

variable "graylog_tcp_ports" {
  type = list
  default = ["9000", "9200", "9300", "22", "27017", "9833", "12201"]
}

variable "sonar_tcp_port" {
  type = list
  default = ["9000"]
}

variable "psql_tcp_port" {
  type = list
  default = ["5432"]
}

variable "os_user" {
  type = string
}

variable "os_pass" {
  type = string
}

variable "def_path" {
  type = string
  default = "general-defaults"
}

variable "ans_path" {
  type = string
  default = "../ansible"
}

variable "def_server_path" {
  type = string
  default = "/var/www"
}

variable "grafana_user" {
  type = string
}

variable "grafana_pass" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "gitlab_access_api_token" {
  type = string
}

variable "pagerduty_user_url" {
  type = string
}

variable "pagerduty_user_key" {
  type = string
}


variable "gitlab_repo" {
  type = string
}

variable "graylog_secret" {
  type = string
}

variable "graylog_password" {
  type = string
}

variable "aws_access_key_id" {
  type = string
}

variable "aws_secret_access_key" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "bucket_key_path_version" {
  type = string
}

variable "meta_name" {
  type = string
}

variable "psql_username" {
  type = string
  default = "sonar"
}

variable "psql_password" {
  type = string
}

variable "sonar_password" {
  type = string
}
