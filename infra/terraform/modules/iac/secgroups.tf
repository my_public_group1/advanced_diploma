# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "asg" {
  name = "Dynamic Security Group for ASG (${terraform.workspace})"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.asg_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.asg_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for ASG (${terraform.workspace})"
  }
}


# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "elb" {
  name = "Dynamic Security Group for ELB (${terraform.workspace})"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.elb_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.elb_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for ELB (${terraform.workspace})"
  }
}

# Созаём правило, которое будет разрешать трафик к нашему узлу управления
resource "aws_security_group" "control_node" {
  name = "Dynamic Security Group for control node (${terraform.workspace})"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.cn_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.cn_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for control node (${terraform.workspace})"
  }
}

resource "aws_security_group" "mon_server" {
  name        = "Monitoring Security Group (${terraform.workspace})"

  dynamic "ingress" {
    for_each = "${local.mon_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    for_each = "${local.mon_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Monitoring Server Security Group (${terraform.workspace})"
  }
}

resource "aws_security_group" "consul_server" {
  name = "Dynamic Security Group for Consul Servers (${terraform.workspace})"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.consul_server_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

 dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${local.consul_server_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for Consul Servers (${terraform.workspace})"
  }
}

resource "aws_security_group" "graylog_server" {
  name        = "GrayLog Security Group (${terraform.workspace})"

  dynamic "ingress" {
    for_each = "${local.graylog_server_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    for_each = "${local.graylog_server_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "GrayLog Server Security Group (${terraform.workspace})"
  }
}

resource "aws_security_group" "sonar_server" {
  name        = "SonarQube Security Group - (${terraform.workspace})"

  dynamic "ingress" {
    for_each = "${local.sonarqube_server_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    for_each = "${local.sonarqube_server_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "SonarQube Server Security Group (${terraform.workspace})"
  }
}
