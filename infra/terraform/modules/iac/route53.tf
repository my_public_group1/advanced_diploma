
# Создадим запись DNS для направления нашего домена на балансер
resource "aws_route53_record" "main" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "${terraform.workspace}.${var.route53_hosted_zone_name}"
  type    = "A"

  alias {
    name                   = "${aws_elb.web.dns_name}"
    zone_id                = "${aws_elb.web.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "monitoring" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "monitoring${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mon_server.public_ip}"]
}

resource "aws_route53_record" "prom" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "prom${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mon_server.public_ip}"]
}

resource "aws_route53_record" "grafana" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "grafana${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mon_server.public_ip}"]
}

resource "aws_route53_record" "alertmanager" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "am${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mon_server.public_ip}"]
}

resource "aws_route53_record" "victoria" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "vicotria${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mon_server.public_ip}"]
}

resource "aws_route53_record" "opensearch" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "os${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "CNAME"
  ttl     = "5"
  records = ["${aws_opensearch_domain.skill_study.endpoint}"]
  depends_on = [aws_opensearch_domain.skill_study]
}

resource "aws_route53_record" "graylog" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "graylog${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.graylog_server.public_ip}"]
}

resource "aws_route53_record" "sonar" {
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "sonar${local.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.sonar_server.public_ip}"]
}
