resource "null_resource" "remove_inventory_file" {
    provisioner "local-exec" {
    command = <<-EOT
      rm -f ${var.ans_path}/consulhosts${terraform.workspace}.inv
      echo '[consul_instances]' >> ${var.ans_path}/consulhosts${terraform.workspace}.inv
    EOT
  }
}

resource "null_resource" "run_ansible_consul_server" {
    provisioner "local-exec" {
    command = <<-EOT
      rm -fr ${var.ans_path}/${var.def_path}${terraform.workspace}
      mkdir ${var.ans_path}/${var.def_path}${terraform.workspace}
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/aws_cli.yml' <<EOL
      aws_access_key_id_input: ${var.aws_access_key_id}
      aws_secret_access_key_input: ${var.aws_secret_access_key}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/aws_s3.yml' <<EOL
      bucket_name: ${var.bucket_name}
      bucket_key_path: ${var.bucket_key_path_version}
      aws_metaname: ${var.meta_name}
      EOL
      cat >'${var.ans_path}/${var.def_path}${terraform.workspace}/aws_region.yml' <<EOL
      aws_region: ${var.aws_region}
      EOL
      ansible-playbook -u ubuntu -i ${var.ans_path}/consulhosts${terraform.workspace}.inv --private-key ${var.ssh_key_private} ${var.ans_path}/consul-servers/main.yml --extra-vars "def_path=${var.def_path} tf_workspace=${terraform.workspace}" -b -v
    EOT
  }
  depends_on = [aws_instance.consul_server]
}


resource "null_resource" "run_ansible_graylog_server" {
    provisioner "local-exec" {
    command = <<-EOT
      ansible-playbook -u ubuntu -i ${var.ans_path}/graylog/grayhosts${terraform.workspace}.inv --private-key ${var.ssh_key_private} ${var.ans_path}/graylog/main.yml --extra-vars "def_path=${var.def_path} tf_workspace=${terraform.workspace}" -b -v
      rm -f ${var.ans_path}/graylog/grayhosts${terraform.workspace}.inv
    EOT
  }
  depends_on = [aws_instance.graylog_server]
}
