variable "bucket_name" {
  type = string
}

variable "bucket_key_path_version" {
  type = string
}

variable "meta_name" {
  type = string
}

variable "aws_region" {
  type = string
}
