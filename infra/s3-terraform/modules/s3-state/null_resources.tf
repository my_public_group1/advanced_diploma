resource "null_resource" "create_tfbackend" {

  depends_on = [aws_s3_bucket.tfstate, aws_dynamodb_table.terraform_locks]

  provisioner "local-exec" {
    command = <<-EOT
      echo '# backend.hcl' > ../terraform/backend.hcl
      cat >>../terraform/backend.hcl <<EOL
      bucket         = "${var.bucket_name}"
      region         = "${var.aws_region}"
      dynamodb_table = "${var.dynamodb_name}"
      key            = "${var.bucket_key_path_state}"      
      encrypt        = true
      EOL
      cat >../terraform/s3.tfvars <<EOL
      aws_region = "${var.aws_region}"
      bucket_name = "${var.bucket_name}"
      bucket_key_path_version = "${var.bucket_key_path_version}"
      meta_name = "${var.meta_name}"
      EOL
    EOT
  }
}
