resource "aws_s3_object" "object_version" {
  bucket = "${var.bucket_name}"
  key    = "${var.bucket_key_path_version}"
  metadata = {
    "x-amz-meta-${var.meta_name}": 1
  }

  depends_on = [aws_s3_bucket.tfstate]
}
