# 1. Monitoring system

Date: 2022-07-20

## Status

Accepted

## Context

We need to create a monitoring system for our server infrastructure deployed on [Amazon AWS](https://aws.amazon.com/).

The monitoring system should be based on Prometheus and related tools.

## Decision

A separate Amazon AWS instance is created for the monitoring system, on which the following tools are installed using Ansible roles:

| Tool | Ansible Role |
| --- | ----------- |
| [Prometheus](https://prometheus.io/) | [cloudalchemy.prometheus](https://github.com/cloudalchemy/ansible-prometheus) |
| [Alertmanager](https://prometheus.io/download/#alertmanager) | [cloudalchemy.alertmanager](https://github.com/cloudalchemy/ansible-alertmanager) |
| [Grafana](https://grafana.com/) | [cloudalchemy.grafana](https://github.com/cloudalchemy/ansible-grafana) |

Various exporters for Prometheus have also been installed on the following servers using Ansible roles:

| Server | Exporter | Ansible Role |
| --- | ----------- | ----------- |
| Monitoring Server | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) | |
| Control Node (gitlab-runner) | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |
|  | [cAdvisor]() | [ome.cadvisor](https://github.com/ome/ansible-role-cadvisor) |
| AWS ASG Servers | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |
|  | [cAdvisor]() | [ome.cadvisor](https://github.com/ome/ansible-role-cadvisor) |
| AWS ELB Server | None | None |

Prometheus also collects metrics from the GO app from the /metrics path.

Alertmanager sends notifications via [PagerDuty](https://www.pagerduty.com/).

Grafana has dashboards for the following metrics:
1. Node Exporter metrics;
2. cAdvisor metrics;
3. GO app metrics;
4. Alertmanager metrics.

The monitoring server responds to the request http://monitoring.example.com:monitoring_tool_port. But also each tool can be accessed by a separate domain:

| Tool | Domain |
| --- | ----------- |
| Prometheus | http://prom.example.com/ |
| Grafana | http://grafana.example.com/ |
| Alertmanager | http://am.example.com/ |

## Consequences

After creating a monitoring system, we will be able to timely monitor the status of our servers and services installed on it, which can help fix the problem in a short time.
