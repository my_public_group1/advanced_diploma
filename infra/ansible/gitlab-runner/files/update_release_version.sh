#! /bin/bash
metadata_string=$(sudo aws s3api head-object --bucket $1 --key $2 | grep "x-amz-meta-$4")
release_version=$(echo "$metadata_string" | grep -Pio '(?<=: ").*(?=")')
printf -v ver_int '%d\n' $release_version 2>/dev/null
new_version_int=$(expr $ver_int + 1)
sudo aws s3api copy-object --bucket $1 --copy-source $1/$2 --key $2 --metadata x-amz-meta-$4=$new_version_int --metadata-directive REPLACE > /dev/null 2>&1
curl --request PUT --show-error "http://$3:8500/v1/kv/$2" --data "$new_version_int" > /dev/null 2>&1
curl "http://$3:8500/v1/kv/$2?raw"
